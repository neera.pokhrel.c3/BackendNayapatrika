<?php
/**
 * Created by PhpStorm.
 * User: neera
 * Date: 4/4/16
 * Time: 4:41 PM
 */
if (!defined('BASEPATH')) exit ('No direct script allowed');

class GCMController extends CI_controller {


    function __construct(){
        parent::__construct();
       $this->load->model('User_model');
//        $this->load->helper('date');
//        $this->load->library('session');
        $this->load->library('gcm');
        $this->load->helper(array('form', 'url'));

    }

    public function listnewstoSend(){
      $data= $this->User_model->getNewsToSend();

		$data2 = array('result' => $data);
		$this->load->view('listnews_tosend',$data2);

    }
    public function sendNews(){
        $sendNewsData= $this->User_model->getNews($_GET['id']);
        $sendNewsData2 = array('news' => $sendNewsData);
        $this->load->view('sendNews_view',$sendNewsData2);


    }

    public function send_gcm()
    {

        if (isset($_POST['sendNews'])) {

            $message = $_POST['newsMessage'];


            // simple adding message. You can also add message in the data,
            // but if you specified it with setMesage() already
            // then setMessage's messages will have bigger priority
            $this->gcm->setMessage($message . date('d.m.Y H:s:i'));


            // add recepient or few
            $this->gcm->addRecepient('fq77mSmtIEE:APA91bG9hBxyYHfqiJRyZTJxG7wihmKl8g0NSvm0XUPYCIZzEZyHbqIJIMlILBD8dizf7nzU3E76DWbL7uKr3CUyrMRyOsnmJ1STvNVFe77W-WEGAQuFf7XRtcLI-KxMMJqYAtw76ZOC');


            // set additional data
            $this->gcm->setData(array(
                'name' => 'Neera'
            ));

            // also you can add time to live
            $this->gcm->setTtl(500);
            // and unset in further
            $this->gcm->setTtl(false);

            // set group for messages if needed
            $this->gcm->setGroup('Test');
            // or set to default
            $this->gcm->setGroup(false);

            // then send
            if ($this->gcm->send())
                echo 'Success for all messages';
            else
                echo 'Some messages have errors';

            // and see responses for more info
            print_r($this->gcm->status);
            print_r($this->gcm->messagesStatuses);

            die(' Worked.');
        }
    }

}