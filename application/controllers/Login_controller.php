<?php
if (!defined('BASEPATH')) exit ('No direct script allowed');

class Login_controller extends CI_controller
{
	function __construct(){
		parent::__construct();
		if ( ! $this->session->userdata('logged_in'))
		{
			// Allow some methods
			$allowed = array(
				'verify_login',
			);
			if ( !in_array($this->router->fetch_method(), $allowed))
			{
				redirect(base_url());
			}

		}
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model');
		$this->load->library('session');



	}


	public function index()
	{
		$this->load->view('admin_view.php');


	}


	public function verify_login()

	{

		if (isset($_POST['mySubmit'])) {
			$email = trim($this->input->post('email'));
			$password = trim($this->input->post('password'));
			$this->load->model('User_model');

			$loginValues = $this->User_model->validate($email, $password);

			if ($loginValues != false) {

				$this->session->set_userdata(array(
					'username' => $loginValues->userName,
					'usertype' => $loginValues->user_type,
					'logged_in' => TRUE
				));
				$message = array(
					'success'=> true,
					'message'=> "You have successfully logged in"
				);

				$this->load->view('admin_view.php',$message);
			} else{
				$message = array(
					'success'=> false
				);

				$this->load->view('login',$message);
			}
		}

	}


	public function addUser()
	{
		$userdata1 = $this->User_model->getUsers();
		$userdata2 = array('user' => $userdata1);
		$this->load->view('add_user',$userdata2);
	}

	public function logout(){
	$this->session->sess_destroy();
		redirect(base_url());
	$this->load->view("login");

	}
	
}
/*
   //This method will have the credentials validation


   $this->load->helper('html');
   $this->load->library('form_validation');
 
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
 
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('login_form');
   }
   else
   {
	   echo redirect("home");
   }
  */ 

   
   