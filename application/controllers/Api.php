<?php
if (!defined('BASEPATH')) exit ('No direct script allowed');

class Api extends CI_controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->library('session');
    }

    public function addGcmToken()
    {

        if (isset($_POST['token'])) {

            $result = $this->User_model->registerGcmToken($_POST['token']);
            echo json_encode($result);
        }

    }

    public function addReportData()
    {

        if (isset($_POST['category'])) {

            $reportData = array(
                "category" => $_POST['category'],
                "date" => $_POST['date']);

            $result = $this->User_model->addIndividualReportData($reportData);
            echo json_encode($result);
            return;
        }

    }


}