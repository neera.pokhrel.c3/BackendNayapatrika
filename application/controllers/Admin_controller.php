<?php
if (!defined('BASEPATH')) exit ('No direct script allowed');

class Admin_controller extends CI_controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->library('gcm');
        $this->load->library('upload');
        if (!$this->session->userdata('logged_in')) {
            // Allow some methods?
            redirect(base_url());
        }
    }

    public function index()
    {

        $this->load->view('admin_view.php');


    }


    public function registerUser()

    {

        if (isset($_POST['userSubmit'])) {
            //set validation rules
            $this->form_validation->set_rules('userName', 'User Name', 'trim|required|min_length[5]|max_length[30]');
            $this->form_validation->set_rules('address', 'Address', 'trim|required|alpha|min_length[5]|max_length[30]');
            $this->form_validation->set_rules('role', 'Role', 'trim|required|alpha|min_length[3]|max_length[30]');
            $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email|is_unique[users_login.email]');
            $this->form_validation->set_rules('contact', 'Phone Number', 'required|numeric|max_length[10]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[cpassword]|md5');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            // check for validation
            if ($this->form_validation->run() == FALSE) {
                $userdata1 = $this->User_model->getUsers();
                $userdata2 = array('user' => $userdata1);
                $this->load->view('add_user',$userdata2);
            } else {
                $userData = array('userName' => trim($this->input->post('userName')),
                    'address' => trim($this->input->post('address')),
                    'user_type' => trim($this->input->post('role')),
                    'email' => trim($this->input->post('email')),
                    'contact' => trim($this->input->post('contact')),
                    'password' => md5($this->input->post('password')));

            $this->User_model->register_user($userData);
            $message = array(
                'success' => true,
                'message' => "User is submitted successfully"
            );
                redirect(base_url("Login_controller/addUser"));
          }
        } else {
            $message = array(
                'success' => false,
                'message' => "User cannot be stored in database"
            );

            $this->load->view('add_user', $message);
        }

    }

    public function addNews()
    {
       $categoryData= $this->User_model->getCategories();
        $categoryData1 = array('category'=>$categoryData);
        $this->load->view('addNews_view',$categoryData1);
    }


    public function insertNews()
    {

        if (isset($_POST['newsSubmit'])) {

//            var_dump($_FILES['userfile']['name']); die();
            $config['upload_path'] = './assets/img';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
            $config['max_size'] = '2048000';
            $config['max_width'] = '1024';
            $config['max_height'] = '768'; 
            $config['overwrite'] = TRUE;

            $newFileName = "";
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $newsData = '';
            if ($this->upload->do_upload()) {
//                $file= $this->upload->data();
                $upload_data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = $upload_data['full_path'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 60;
                $config['height'] = 60;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();

                $newsData = array('upload' => $this->upload->data());

                $newFileName = base_url('assets/img') . '/' .  $newsData['upload']['file_name'];

            }
            $newsData = array('title' => trim($this->input->post('nTitle')),
                'contentencoded' => trim($this->input->post('ndescription')),
                'author' => trim($this->input->post('nauthor')),
                'date' => date('d M Y H:i:s'),
                'image' => $newFileName,
                'category' => trim($this->input->post('ncategory')));
            $this->User_model->fillNews($newsData);

            $this->send_gcm($newsData);
            $categoryData= $this->User_model->getCategories();
            $categoryData1 = array('category'=>$categoryData,
                'success' => true);
            $this->load->view('addNews_view',$categoryData1);
        } else {
            $message = array(
                'success' => false,
                'message' => "User cannot be stored in database"
            );

            $this->load->view('addNews_view');
        }
    }


    public function listNews()
    {

        $data = $this->User_model->getNewsItems();

        $data2 = array('result' => $data);
        $this->load->view('listNews_view', $data2);

    }


    public function editUser()
    {
        $editData = $this->User_model->getUser($_GET['id']);
        $editData2 = array('user' => $editData);
        $this->load->view('editUser_view', $editData2);
    }

    public function insertEditedUser()
    {
        if (isset($_POST['editUserSubmit'])) {
            $editeduserData = array('userName' => trim($this->input->post('userName')),
                'user_Id' => trim($this->input->post('id')),
                'address' => trim($this->input->post('address')),
                'user_type' => trim($this->input->post('role')),
                'email' => trim($this->input->post('email')));;
            $this->User_model->update_user($editeduserData);
            $message = array(
                'success' => true,
                'message' => "User is edited successfully"
            );
            redirect(base_url("Login_controller/addUser"), $message);
        } else {
            $message = array(
                'success' => false,
                'message' => "User cannot be edited in database"
            );

            $this->load->view('editUser_view', $message);
        }

    }


    public function deleteUser()
    {
        $this->User_model->deleteUser($_GET['id']);
        redirect(base_url("Login_controller/addUser"));
    }

    public function editNews()
    {
        $editNewsData = $this->User_model->getNews($_GET['id']);
        $editNewsData2 = array('news' => $editNewsData);
        $this->load->view('editNews_view', $editNewsData2);
    }

    public function insertEditedNews()
    {
        if (isset($_POST['editedNewsSubmit'])) {
            $config['upload_path'] = './assets/img';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '100';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $config['overwrite']  = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $config['image_library'] = 'gd2';
                $config['source_image'] = $upload_data['full_path'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 60;
                $config['height'] = 60;

                $this->load->library('image_lib', $config);

                $this->image_lib->resize();


            }
            $editedFileName = base_url('assets/img') . '/' . $this->upload->data()["file_name"];
            $editednewsData = " ";
            $editednewsData = array('title' => trim($this->input->post('nTitle')),
                'id' => trim($this->input->post('newsId')),
                'contentencoded' => trim($this->input->post('ndescription')),
                'author' => trim($this->input->post('nauthor')),
                'image' => $editedFileName,
                'category' => trim($this->input->post('ncategory')));
            $this->User_model->updateNews($editednewsData);

            $message = array(
                'success' => true,
                'message' => "News is submitted successfully"
            );
            redirect(base_url("Admin_controller/listNews"));
        } else {
            $message = array(
                'success' => false,
                'message' => "User cannot be stored in database"
            );

            $this->load->view('addNews_view');
        }
    }

    public function deleteNews()
    {
        $this->User_model->deleteNews($_GET['id']);
        redirect(base_url("Admin_controller/listNews"));
    }


    private function send_gcm($newsData)
    {
    	//var_dump($newsData); 
        /*
                $editednewsData = array('title' => trim($this->input->post('nTitle')),
                    'id' => trim($this->input->post('newsId')),
                    'contentencoded' => trim($this->input->post('ndescription')),
                    'author' => trim($this->input->post('nauthor')),
                    'image' => $editedFileName,
                    'category' => trim($this->input->post('ncategory')));*/


        $result = $this->User_model->getAllGcmToken();
       // var_dump($result);

        // adding recepient or for sending gcm
        for ($i = 0; $i < sizeof($result); $i++) {
            $this->gcm->addRecepient($result[$i]->registrationToken);
        }

        // setting additional data
        $this->gcm->setData($newsData);

        // then sending
        $this->gcm->send();

        // responses for more info
    // print_r($this->gcm->status);
    // print_r($this->gcm->messagesStatuses);
    // die();


        $data['success'] = "News successfully sent!";
        $this->load->view('addNews_view', $data, true);


    }

    public function  generateMonthlyReport()
    {
        $this->load->view('monthlyreport');


    }

    public function  generateDailyReport()
    {
        $this->load->view('dailyreport');


    }

    public function  generateWeeklyReport()
    {
        $this->load->view('weeklyreport');


    }

    public function  generateQuaterlyReport()
    {
        $this->load->view('quarterlyreport');


    }

    public function  generateYearlyReport()
    {
        $this->load->view('yearlyreport');


    }


    public function getWeeklyReport()
    {

//        $response =   $this->User_model->getReportTest();
        $response =   $this->User_model->getWeeklyReport();
        echo json_encode($response);

    }



    public function getDailyReport()
    {
        $response =   $this->User_model->getDailyReport();
        echo json_encode($response);

    }



    public function getMonthlyReport()
    {

        $response =   $this->User_model->getMonthlyReport();
        echo json_encode($response);

    }

    public function getYearlyReport()
    {

        $response =   $this->User_model->getYearlyReport();
        echo json_encode($response);

    }


}

/*
   //This method will have the credentials validation


   $this->load->helper('html');
   $this->load->library('form_validation');
 
   $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
   $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
 
   if($this->form_validation->run() == FALSE)
   {
     //Field validation failed.  User redirected to login page
     $this->load->view('login_form');
   }
   else
   {
	   echo redirect("home");
   }
  */ 
