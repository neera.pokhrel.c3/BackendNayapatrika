<?php

class User_model extends CI_Model
{

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function validate($email, $password)
    {
        $sql = "select * from users_login where email = '" . $email . "' and password = '" . $password . "'";
        $query = $this->db->query($sql);
        $row = $query->row();
        if ($row) {

            return $row;
        } else {

            return false;
        }
    }

    public function register_user($userdata)
    {
        $this->db->insert('users_login', $userdata);
        return true;

    }

    public function update_user($editeduserdata)
    {
        $this->db->where('user_Id', $editeduserdata['user_Id']);
        $this->db->update('users_login', $editeduserdata);
        return true;

    }

    public function deleteUser($id)
    {
        $this->db->where('user_Id', $id);
        $this->db->delete('users_login');
        return true;

    }

    public function fillNews($newsData)
    {
        $this->db->insert('newsItems', $newsData);
        return true;

    }

    public function updateNews($editednewsdata)
    {
        $this->db->where('id', $editednewsdata['id']);
        $this->db->update('newsItems', $editednewsdata);
        return true;
    }

    public function getCategories(){
        $sql = "Select * from category ";
        $query = $this->db->query($sql);
        $row = $query->result();

        if ($row) {
            return $row;
        } else {
            return false;
        }

    }


    public function getNewsItems()
    {
        $sql = "Select * from newsItems ";
        $query = $this->db->query($sql);
        $row = $query->result();

        if ($row) {
            return $row;
        } else {
            return false;
        }

    }

    public function getUsers()
    {
        $sql = "Select * from users_login ";
        $query = $this->db->query($sql);
        $row = $query->result();

        if ($row) {
            return $row;
        } else {
            return false;
        }

    }

    public function getUser($id)
    {
        $sql = "Select * from users_login WHERE user_Id=$id ";
        $query = $this->db->query($sql);
        $row = $query->result();

        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

    public function getNews($id)
    {
        $sql = "Select * from newsItems WHERE id=$id ";
        $query = $this->db->query($sql);
        $row = $query->result();

        if ($row) {
            return $row;
        } else {
            return false;
        }
    }

    public function deleteNews($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('newsItems');
        return true;

    }

    public function getNewsToSend()
    {
        $sql = "SELECT *
              FROM newsItems
              WHERE sentStatus =0
              ORDER BY date DESC
              LIMIT 20";
        $query = $this->db->query($sql);
        $row = $query->result();

        if ($row) {
            return $row;
        } else {
            return false;
        }

    }


    public function registerGcmToken($token)
    {

        $sql = "SELECT * FROM registration WHERE registrationToken = ?";
        $query = $this->db->query($sql, array($token));
        $result = $query->result();
        if (!sizeof($result) > 0) {
            $this->db->insert('registration', array('registrationToken' => $token));
        }
        return "success";

    }


    public function getReportTest()
    {

        $sql = "SELECT date as year, COUNT(category) as value FROM  reportData GROUP BY category";
        $q = $this->db->query($sql);
        $rows = $q->result_array();

        return $rows;
    }

    public function  getWeeklyReport()
    {
        $sql = "SELECT category, COUNT(category) as count FROM reportData
                WHERE
                WEEK (date) = WEEK( current_date ) - 1 AND YEAR( date) = YEAR( current_date ) GROUP BY category";

        $q = $this->db->query($sql);
        $rows = $q->result_array();

        return $rows;

    }

    public function  getMonthlyReport()
    {
        $sql = "SELECT
                    category,
                    COUNT(category) as count
                FROM
                    reportData
                Where
                    date >= Subdate(
                        Curdate(),
                        INTERVAL 1 MONTH
                    )
                GROUP BY
                    category;";

        $q = $this->db->query($sql);
        $rows = $q->result_array();

        return $rows;

    }

    public function  getDailyReport()
    {
        $sql = "SELECT
                    category,
                    COUNT(category)  as count
                FROM
                    reportData
                Where
                    DATE(date) = Curdate()
                GROUP BY
                    category;";

        $q = $this->db->query($sql);
        $rows = $q->result_array();

        return $rows;

    }

    public function  getYearlyReport()
    {
        $sql = "SELECT
                    category,
                    COUNT(category) as count
                FROM
                    reportData
                Where
                    Year(date) = Year(current_date)
                GROUP BY
                    category";

        $q = $this->db->query($sql);
        $rows = $q->result_array();

        return $rows;

    }

    public function getAllGcmToken()
    {
        $sql = "SELECT registrationToken FROM registration ";
        $query = $this->db->query($sql);
        $result = $query->result();

        return $result;
    }


    public function addIndividualReportData($reportData)
    {

        $this->db->insert('reportData', $reportData);
        return true;
    }

}
  