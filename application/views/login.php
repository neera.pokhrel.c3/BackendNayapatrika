<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
       

</head>
<body>
  <div class="top-content">         
    <div class="inner-bg">
        <div class="container">
            <div class="col-sm-4 col-sm-offset-4 text">
                <div class="form-bottom" style="background-color:#FAFAFA ; margin-top:47px">
                    <div class="col-sm-2 col-sm-offset-2 text">

            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
                        <img src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="..." class="img-circle">
                        <p id="profile-name" class="profile-name-card"></p>
                    </div>


                    <form class="form-signin" action="<?= site_url('Login_controller/verify_login')?>" method="post">
<br/> <br/>

                
                        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
                        
                        <br/>
                         <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
                        
                        <div id="remember" class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                         </div>
                        <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit" name="mySubmit">Sign in</button>

                    </form><!-- /form -->
                    <a href="#" class="forgot-password">
                         Forgot the password?
                    </a>
                    <?php if (isset($success)) {
                        if (!$success) { ?>
                            <p style="color: #CD5C5C;"><?php echo "Username password doesn't match!" ?></p>
                        <?php }
                    } ?>

                </div>


                </div>
            </div><!-- /card-container -->
        </div> 
    </div><!-- /container -->
  
</body>
</html>
