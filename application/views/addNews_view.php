
<?php
 include('include/header.php');
?>


<div id="page-wrapper">
    <div id="page-inner">
        <div class="row" align="left">
            <div class="col-md-5">
                <h2>Add News Items</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-10">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <?php if (isset($success)) {
                        if ($success) { ?>
                            <p style="color: #009926;"><?php echo "News successfully sent!" ?></p>
                        <?php }
                    } ?>
                    <div class="panel-heading" align="left">
                        Add or Upload News Items Here
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                            <?php echo form_open_multipart('Admin_controller/insertNews');?>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                        <input type="text" class="form-control" name="nTitle" id="inputEmail3"
                                               placeholder="Enter News Title Here" required>
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                        <textarea class="form-control" name="ndescription"
                                                  placeholder="Enter Description Here" rows="3" required></textarea>
                                    </div>
                                    <div class="form-group input-group">
                                        <label for="exampleInputFile" class="col-sm-2 control-label">Image</label>

                                        <div class="col-sm-10">
                                            <input type="file" id="exampleInputFile" name="userfile" >

                                            <p class="help-block" align="left">Upload Images here</p>
                                        </div>
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>

                                        <input type="text" class="form-control" name="nauthor" id="inputEmail3"
                                               placeholder=" Enter Author Here">
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="ncategory">
                                            <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                                            <option selected="selected">Select a category</option>

                                            <?php
                                            foreach($category as $name) { ?>
                                                <option value="<?php echo $name->category;?>">
                                                <?php echo $name->category;?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-sm-2 control-label"></label>

                                        <div class="col-sm-1">
                                            <button type="submit" name="newsSubmit" class="btn btn-primary">Submit
                                            </button>
                                        </div>
                                    </div>

                                 <?php echo "</form>"?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('include/footer.php'); ?>
