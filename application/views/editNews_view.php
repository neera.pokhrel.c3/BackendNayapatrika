

<?php include('include/header.php');?>
/**
 * Created by PhpStorm.
 * User: neera
 * Date: 4/5/16
 * Time: 3:44 PM
 */




<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row" align="left">
            <div class="col-md-5">
                <h2>Edit News Items</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-10">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading" align="left">
                        Edit News Details Here
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="<?=site_url('Admin_controller/insertEditedNews')?>" method="post">
                                    <?php foreach($news as $news):{?>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text" class="form-control" name="newsId" id="inputEmail3" value="<?php echo $news->id;?>" readonly>
                                        </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                        <input type="text" class="form-control" name="nTitle" id="inputEmail3" value="<?php echo $news->title;?>">
                                    </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                        <textarea class="form-control" name="ndescription"  rows="3"><?php echo $news->contentencoded;?></textarea>
                                    </div>
                                        <div class="form-group input-group">
                                            <label for="exampleInputFile" class="col-sm-2 control-label">Image</label>
                                            <div class="col-sm-10">
                                                <input type="file" value="<?php echo $news->image;?>" id="exampleInputFile" multiple accept='image/*' name="userfile" required>
                                                <p class="help-block" align="left">Upload Images here</p>
                                            </div>
                                        </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>

                                        <input type="text" class="form-control" name="nauthor" id="inputEmail3" value="<?php echo $news->author;?>">
                                    </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>

                                            <input type="text" class="form-control" name="ncategory" id="inputEmail3" value="<?php echo $news->category;?>">
                                        </div>


                                    <?php }endforeach; ?>
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-1">
                                            <button type="submit" name="editedNewsSubmit" class="btn btn-primary">Edit</button>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('include/footer.php');?>
