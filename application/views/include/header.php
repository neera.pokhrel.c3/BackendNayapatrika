
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> Dashboard Page : </title>
	<!-- BOOTSTRAP STYLES-->
    <link href= "<?php echo site_url('assets/css/bootstrap.css'); ?>" rel="stylesheet"/>

     <!-- FONTAWESOME STYLES-->
    <link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css' />



     <!-- MORRIS CHART STYLES-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


<!--    <link href="--><?php //echo site_url('assets/js/morris/morris-0.4.3.min.css'); ?><!--" rel="stylesheet" />-->
        <!-- CUSTOM STYLES-->
    <link href="<?php echo site_url('assets/css/custom.css'); ?>" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">NayaPatrika</a>
            </div>
  <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">  <a href="<?php echo base_url('Login_controller/logout');?>" class="btn btn-danger square-btn-adjust">Logout</a> </div>
        </nav>
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="<?php echo site_url('assets/img/logo1.jpg'); ?>" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a class="active-menu"  href="<?php echo base_url('Login_controller/index');?>"> <i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
                     <li>
                     <!--   <a href="<?php echo base_url('Login_controller/addUser');?>"> <i class="fa fa-laptop fa-3x"></i> Register User </a> -->
                    </li>
					                   
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i> Manage News<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo base_url('Admin_controller/addNews');?>">Add news item to Send</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('Admin_controller/listNews');?>">List news items</a>
                            </li>
                            <li>
                                
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>

                                </ul>
                               
                            </li>
                        </ul>
                      </li>  
                  <li>
                        <a href="#"><i class="fa fa-sitemap fa-3x"></i> Generate Reports<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="<?php echo base_url('Admin_controller/generateDailyReport');?>">
                                Daily Report</a>
                            </li>
                            <li>
                            <a href="<?php echo base_url('Admin_controller/generateWeeklyReport');?>">Weekly Report</a>
                            
                            </li>
                            <li>
                            <a href="<?php echo base_url('Admin_controller/generateMonthlyReport');?>">
                                Monthly Report</a>
                             </li>

                            <li>
                                <a href="<?php echo base_url('Admin_controller/generateQuaterlyReport');?>">Quaterly Report</a>


                            </li>

                            <li>
                                <a href="<?php echo base_url('Admin_controller/generateYearlyReport');?>">Yearly Report</a>


                            </li>
                        </ul>
                      </li

               
            </div>
            
        </nav> 
