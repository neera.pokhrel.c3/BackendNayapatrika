<?php include('include/header.php');?>

<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Generate Daily report</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr />

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Daily Report:
                    </div>
                    <div class="panel-body">
                        <div id="morris-bar-custom"></div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /. ROW  -->

        <!-- /. ROW  -->
    </div>
    <!-- /. PAGE INNER  -->
</div>


   <script type="text/javascript">

        $.ajax({
            url: '<?php echo  base_url('Admin_controller/getDailyReport') ;?>',
            dataType: 'json',
            type: 'post',
            success: function (response) {

                var chart = new Morris.Bar({
                    element: 'morris-bar-custom',
                    data: [
                        {
                            category: null, count: null
                        }
                    ],
                    xkey: 'category',
                    ykeys: ['count'],
                    labels: ['Number Of Views']
                });
                chart.setData(response);
            }
        });


    </script>

<!-- /. PAGE WRAPPER  -->
</div><?php include('include/footer.php');?>