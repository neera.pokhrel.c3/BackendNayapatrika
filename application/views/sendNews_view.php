

<?php include('include/header.php');?>
/**
* Created by PhpStorm.
* User: neera
* Date: 4/5/16
* Time: 3:44 PM
*/




<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row" align="left">
            <div class="col-md-5">
                <h2>Edit Message Before Sending</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-10">
                <!-- Form Elements -->
                <div class="panel panel-default">
                    <div class="panel-heading" align="left">
                        Edit News Details Here
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" action="<?=site_url('GCMController/send_gcm')?>" method="post">
                                    <?php foreach($news as $news):{?>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text" class="form-control" name="newsId" id="inputEmail3" value="<?php echo $news->id;?>" readonly>
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text" class="form-control" name="newsMessage" id="inputEmail3" value="<?php echo $news->title;?>">
                                        </div>

                                    <?php }endforeach; ?>
                                    <div class="form-group">
                                        <label for="inputTitle" class="col-sm-2 control-label"></label>
                                        <div class="col-sm-1">
                                            <button type="submit" name="sendNews" class="btn btn-primary"> Send Notofication</button>
                                        </div>
                                    </div>


                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('include/footer.php');?>
