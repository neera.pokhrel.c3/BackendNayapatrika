<?php include('include/header.php');?>
<div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>List News Items</h2>
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />

            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        News List
                      </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Edit</th>
                                    <th>Delete</th>

                                <?php foreach($result as $results):{?>
                                    <tr>

<!--                                        --><?php //var_dump($results->news_id);die();?>
                                    <td><?php echo $results->id;?></td>
                                    <td><?php echo $results->title;?></td>
                                    <td><?php echo $results->contentencoded;?></td>
                                    <td><img src="<?php echo $results->image?>"></td>
                                    <td><?php echo $results->date;?></td>
                                    <td><?php echo $results->category;?></td>

                                       <!-- <td><a href="<?= base_url();?>GCMController/send_gcm" class="btn btn-warning">Send Notification</a> </td> -->
                                       <td><a href="<?= base_url();?>Admin_controller/editNews?id=<?=$results->id;?>" class="btn btn-info">Edit</a> </td>
                                      <td> <a href="<?= base_url();?>Admin_controller/deleteNews?id=<?=$results->id;?>" class="btn btn-danger"  onclick="return confirm
                  ('Are you sure  to Delete?')">Delete</a></td>
                                            </tr>
                                    </tr>
                                <?php }endforeach; ?>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!--End Advanced Tables -->
                    <
                </div>
            </div>
          </div>
    </div>

<?php include('include/footer.php');?>