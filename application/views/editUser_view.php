
<?php include('include/header.php');?>


<!-- <p><?php echo $this->session->flashdata('errorMsg'); ?></p> -->

<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Admin Dashboard</h2>
                <h4><strong>Edit User Details</strong>. </h4>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>Edit User </strong>
                    </div>
                    <div class="panel-body">
                        <?php if(isset($success)){ if($success){ ?>
                            <p color ="green"><?php echo $message ?></p>
                        <?php } } ?>
                        <form role="form" action="<?=site_url('Admin_controller/insertEditedUser')?>" method="post" >
                            <br/>
                            <?php foreach($user as $users):{?>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-circle-o-notch"  ></i></span>
                                    <input type="text" name="id" class="form-control" value="<?php echo $users->user_Id;?>" readonly />
                                </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-circle-o-notch"  ></i></span>
                                <input type="text" name="userName" class="form-control" value="<?php echo $users->userName;?>" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                <input type="text"name="address" class="form-control" value="<?php echo $users->address;?>" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">@</span>
                                <input type="email"name="email" class="form-control" value="<?php echo $users->email;?>" />
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                <input type="text"name="role" class="form-control" value="<?php echo $users->user_type;?>" />
                            </div>

                            <button class="btn btn-primary" type="submit" name="editUserSubmit">Edit User</button>
                            <hr />
                            <?php }endforeach; ?>
                        </form>
                    </div>

                </div>
            </div>
<?php include('include/footer.php');?>