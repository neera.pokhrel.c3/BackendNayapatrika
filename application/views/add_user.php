 <?php include('include/header.php');?>

      
        <!-- <p><?php echo $this->session->flashdata('errorMsg'); ?></p> -->
 
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Admin Dashboard</h2>   
                        <h4>Welcome <strong>Add new user here</strong>. </h4>
                    </div>
                </div>  
             <div class="row">
               
                <div class="col-md-12 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>  Register User </strong>  
                            </div>
                            <div class="panel-body">
                                <?php if(validation_errors()) { ?>
                                    <div class="alert alert-warning">
                                        <?php echo validation_errors(); ?>
                                    </div>
                                <?php } ?>
                              <?php if(isset($success)){ if($success){ ?>
                                <p color ="green"><?php echo $message ?></p>
                             <?php } } ?>
                                <?php echo form_open('Admin_controller/registerUser');?>
                                <br/>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-circle-o-notch"  ></i></span>
                                            <input type="text" name="userName" class="form-control" placeholder="Enter User Name" />
                                        </div>
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text"name="address" class="form-control" placeholder=" Enter Address" />
                                        </div>
                                         <div class="form-group input-group">
                                            <span class="input-group-addon">@</span>
                                            <input type="email"name="email" class="form-control" placeholder=" Enter Email" />
                                        </div>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                            <input type="text"name="role" class="form-control" placeholder=" Enter Role" />
                                        </div>
                                    <div class="form-group input-group">
                                        <span class="input-group-addon"><i class="fa fa-tag"  ></i></span>
                                        <input type="text"name="contact" class="form-control" placeholder=" Enter Phone" />
                                    </div>

                                      <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" name="password" class="form-control" placeholder="Enter Password" />
                                        </div>
                                     <div class="form-group input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock"  ></i></span>
                                            <input type="password" name="cpassword" class="form-control" placeholder="Confirm Password" />
                                        </div>
                                     
                                     <button class="btn btn-primary" type="submit" name="userSubmit">Register User</button>
                                    <hr />

                                <?php echo form_close(); ?>
                            </div>
                           
                        </div>
                    </div>
                
             </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Registered User List
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Address</th>
                                        <th>Phone No</th>
                                        <th>Edit</th>
                                        <th>Delete</th>

                                        <?php foreach($user as $users):{?>
                                            <tr>
                                                <!--                                        --><?php //var_dump($results->news_id);die();?>
                                                <td><?php echo $users->user_Id;?></td>
                                                <td><?php echo $users->userName;?></td>
                                                <td><?php echo $users->email;?></td>
                                                <td><?php echo $users->user_type;?></td>
                                                <td><?php echo $users->address;?></td>
                                                <td><?php echo $users->contact;?></td>
                                                <td><a href="<?= base_url();?>Admin_controller/editUser?id=<?=$users->user_Id;?>" class="btn btn-info">Edit</a> </td>
                                                <td><a href="<?= base_url();?>Admin_controller/deleteUser?id=<?=$users->user_Id;?>" class="btn btn-danger"  onclick="return confirm
                  ('Are you sure  to Delete?')">Delete</a></td>
                                            </tr>
                                        <?php }endforeach; ?>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!--End Advanced Tables -->
                    </div>
                </div>
    </div>

    <?php include('include/footer.php');?>
